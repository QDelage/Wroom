/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Récupérer les premieres lettres des noms de chaque pilotes présents dans la base
 * @return Un tableau qui contient la lettre
 */
module.exports.getLettresPilotes = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT distinct substr(pilnom,1,1) as lettre FROM pilote " +
                "ORDER BY 1";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer les premieres lettres des noms de chaque pilotes présents dans la base
 * @return Un tableau qui contient la lettre
 */
module.exports.getPilotesParLettre = function(lettre, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT pi.pilnum, pilprenom, pilnom, phoadresse FROM pilote pi " +
                "JOIN photo ph ON pi.pilnum = ph.pilnum " +
                "WHERE pilnom LIKE '" + lettre + "%' " +
                "AND phonum = 1 " +
                "ORDER BY 1";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer tous les pilotes
 * @return tous les pilotes
 */
module.exports.getAllPilotes = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT pi.pilnum, pilprenom, pilnom, DATE_FORMAT(pildatenais, \"%d/%m/%Y\") as dateNais FROM pilote pi " +
                "ORDER BY pilnom";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir les infos d'un pilote selon son id
 */
module.exports.getPiloteParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT pi.pilnum, pilprenom, pilnom, pilpoints, phoadresse, " +
                "pilpoids, piltaille, paynom, piltexte,  " +
                "DATE_FORMAT(pildatenais, \" %d/%m/%Y \") as pildatenais " +
                "FROM pilote pi " +
                "JOIN photo ph ON pi.pilnum = ph.pilnum " +
                "JOIN pays pa ON pi.PAYNUM = pa.PAYNUM " +
                "WHERE pi.pilnum = " + id + " ";
            connexion.query(sql, callback);




            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};



/*
 * Obtenir les sponsors d'un pilote
 */
module.exports.getSponsorsParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT sp.* FROM pilote p " +
                "JOIN sponsorise s ON p.PILNUM = s.PILNUM " +
                "JOIN sponsor sp ON s.SPONUM = sp.SPONUM " +
                "WHERE p.pilnum = " + id + " ";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir les pays
 */
module.exports.getAllPays = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT paynum, paynom, paynat, payadrdrap " +
                "FROM pays s " +
                "ORDER BY paynom";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Ajouter un pilote
 */
module.exports.ajouterPilote = function(pilote, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL            
            let sql = "INSERT INTO pilote (PILPRENOM, PILNOM, PILDATENAIS, PAYNUM, ECUNUM, PILPOINTS, PILPOIDS, PILTAILLE ,PILTEXTE) VALUES " +
                '("' + pilote.prenom + '", "' + pilote.nom + '", STR_TO_DATE("' + pilote.dateNais + '\",\"%d/%m/%Y\"), \"' + pilote.natio + '", ' +
                pilote.ecurie + ', ' + pilote.points + ', ' + pilote.poids + ', '+ pilote.taille +', "' + pilote.desc + '")';
            
            // STR_TO_DATE convertit la chaine de caractère en date SQL
            
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Modifier un pilote
 */
module.exports.modifierPilote = function(pilote, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL            
            let sql = 'UPDATE pilote SET PILPRENOM = "' + pilote.prenom +
                '", PILNOM = "'+ pilote.nom +
                '", PILDATENAIS = STR_TO_DATE("' + pilote.dateNais + '\",\"%d/%m/%Y\")' +
                ', PAYNUM = ' + pilote.natio +
                ', ECUNUM = ' + pilote.ecurie +
                ', PILPOINTS = ' + pilote.points +
                ', PILPOIDS  = ' + pilote.poids +
                ', PILTAILLE = ' + pilote.taille +
                ', PILTEXTE = "' + pilote.desc + '" ' +
                ' WHERE pilnum = ' + pilote.num;
            
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer un pilote
 */
module.exports.supprimerPilote = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM pilote ' +
                'WHERE pilnum = ' + id;             
            
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Effacer les écuries des pilotes dont on va supprimer une écurie
 */
module.exports.effacerEcurieDesPilotes = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'UPDATE pilote SET ' +
                'ECUNUM = NULL ' 
                'WHERE ecunum = ' + id;
            
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};