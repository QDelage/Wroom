/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Supprimer toutes les courses d'un pilote
 */
module.exports.supprimerCoursesByIdPilote = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM course ' +
                'WHERE pilnum = ' + id;

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer toutes les courses des GP d'un circuit
 */
module.exports.supprimerCoursesParIdCircuit = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE course FROM course ' +
                'INNER JOIN grandprix gp ON course.gpnum = gp.gpnum ' +
                'INNER JOIN circuit ci ON ci.cirnum = gp.cirnum ' +
                'WHERE ci.cirnum = ' + id;            

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Ajouter résultat d'une course
 * @return
 */
module.exports.ajouterResultatCourse = function(resultat, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "INSERT INTO course SET " +
                " GPNUM = " + resultat.gpnum +
                ", PILNUM = " + resultat.pilnum +
                ", TEMPSCOURSE = \"" + resultat.tempscourse + "\"";

            connexion.query(sql, callback);
            

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer le résultat d'un pilote sur une course
 * @return
 */
module.exports.supprimerResultatCourse = function(id, gp, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "DELETE FROM course WHERE " +
                " GPNUM = " + gp +
                " AND PILNUM = " + id;

            connexion.query(sql, callback);
            

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};