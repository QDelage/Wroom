/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Supprimer toutes les relations de sponsoring d'un pilote
 */
module.exports.supprimerSponsorsDUnPilote = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM sponsorise ' +
                'WHERE pilnum = ' + id;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer toutes les relations de sponsoring d'un sponsor
 */
module.exports.supprimerPiloteSponsoriseesParSponsor = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM sponsorise ' +
                'WHERE sponum = ' + id;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer toutes les relations de financement d'une écurie
 */
module.exports.supprimerFinancementDUneEcurie = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM finance ' +
                'WHERE ecunum = ' + id;
                
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer toutes les relations de financement d'un sponsor
 */
module.exports.supprimerFinancementDUnSponsor = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM finance ' +
                'WHERE sponum = ' + id;
                
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer un sponsor
 */
module.exports.supprimerSponsor = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM sponsor ' +
                'WHERE sponum = ' + id;
                
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Lister tous les sponsors
 */
module.exports.getAllSponsors = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'SELECT sponum, sponom, sposectactivite  ' +
                'FROM sponsor ' +
                'ORDER BY sponom';
                
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir un sponsor avec son id
 */
module.exports.getSponsorParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'SELECT sponum, sponom, sposectactivite  ' +
                'FROM sponsor ' +
                'WHERE sponum = ' + id;
                
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir l'écurie que finance un sponsor
 */
module.exports.getEcurieFinancee = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'SELECT ecunum  ' +
                'FROM finance ' +
                'WHERE sponum = ' + id;
                
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Ajouter sponsor
 */
module.exports.ajouterSponsor = function(sponsor, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'INSERT INTO sponsor SET ? ';
                
            connexion.query(sql, sponsor, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Ajouter une relation de financement d'un sponsor à une écurie
 */
module.exports.ajouterFinancement = function(financement, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'INSERT INTO finance SET ? ';
                
            connexion.query(sql, financement, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Modifier un sponsor
 */
module.exports.modifierSponsor = function(sponsor, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL            
            let sql = 'UPDATE sponsor SET ' +
                ' SPONOM = "' + sponsor.sponom +
                '", SPOSECTACTIVITE = "'+ sponsor.sposectactivite +
                '" WHERE sponum = ' + sponsor.sponum;
            
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Modifier un financement
 */
module.exports.modifierFinancement = function(financement, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL            
            let sql = 'UPDATE finance SET ' +
                ' ecunum = ' + financement.ecunum +
                ' WHERE sponum = ' + financement.sponum;
                            
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};