/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Obtenir les photos d'un pilote selon son id
 */
module.exports.getPhotosParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT phonum, phoadresse, phosujet, phocommentaire " +
                "FROM pilote pi JOIN photo ph ON pi.pilnum = ph.pilnum " +
                "WHERE pi.pilnum = " + id + " ";
            connexion.query(sql, callback);



            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer toutes les photos d'un pilote
 */
module.exports.supprimerPhotosParIdPilote = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM photo ' +
                'WHERE pilnum = ' + id;

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Supprimer une photo d'un pilote
 */
module.exports.supprimerPhoto = function(photo, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'DELETE FROM photo ' +
                ' WHERE pilnum = ' + photo.pilnum +
                " AND phonum = " + photo.phonum;            

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Ajouter une photo avec toutes ses données (dont l'id)
 */
module.exports.ajouterPhoto = function(image, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'INSERT INTO photo SET ?';

            connexion.query(sql, image, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir la place d'une photo d'un pilote à ajouter ensuite
 */
module.exports.getPlacePhoto = function(pilnum, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = ' SELECT MAX(phonum) + 1 AS id FROM photo ' +
                    'WHERE pilnum = ' + pilnum;            

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Modifier une photo
 */
module.exports.modifierPhoto = function(image, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'UPDATE photo SET ' +
                ' phoadresse = "' + image.phoadresse + '"';

            if (null != image.phosujet) {
                sql += ', phosujet = "' + image.phosujet + '"';
            }

            if (null != image.phocommentaire) {
                sql += ', phocommentaire = "' + image.phocommentaire + '"';
            }

            sql += " WHERE PILNUM = " + image.pilnum;
            sql += " AND PHONUM = " + image.phonum;            

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Lister les photos d'un pilote
 */
module.exports.listerPhotosPilote = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = 'SELECT phonum, phosujet, phocommentaire, phoadresse' +
                ' FROM photo WHERE pilnum = ' + id;                

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};