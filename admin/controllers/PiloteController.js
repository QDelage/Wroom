// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S
let model = require('../models/pilote.js');
let modelEcurie = require('../models/ecurie.js');
let modelPhotos = require('../models/photos.js');
let modelCourse = require('../models/course.js');
let modelSponsor = require('../models/sponsor.js');

const fs = require('fs');

let async = require('async');

module.exports.AffichagePilotes = function(request, response) {
    response.title = 'Gestion des pilotes';


    async.parallel([
        function(callback) {
            model.getAllPilotes(function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.pilotes = result[0];
        
        response.render('gestionPilotes', response);

    });

};

module.exports.AjoutForm = function(request, response) {
    response.title = 'Ajout d\'un pilote';

    let id = request.params.num; // Id du pilote à modifier

    async.parallel([
        // result[0]
        function(callback) {
            model.getAllPays(function(err, result) {
                callback(null, result);
            });
        },
        // result[1]
        function(callback) {
            modelEcurie.getListeEcurie(function(err, result) {
                callback(null, result);
            });
        },
        // result[2]
        function(callback) {
            model.getPiloteParId(id, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.pays = result[0];
        response.ecuries = result[1];
        if (result[2]) {

            // Suppression espaces autour le cas échéant
            let date = result[2][0].pildatenais.split(" ").join("");
                        
            
            response.pilote = result[2][0];
            response.dateJour = date.split("/")[0];
            response.dateMois = date.split("/")[1];
            response.dateAnnee = date.split("/")[2];
        }

        
        response.render('gestionPilotes', response);

    });
 

};

module.exports.AjouterPilote = function(request, response) {
    response.title = 'Ajout d\'un pilote';

    let pilote = {
        "prenom":request.body.prenom,
        "nom":request.body.nom,
        "dateNais":request.body.dateNaisJour + "/" + request.body.dateNaisMois + "/" + request.body.dateNaisAnnee,
        "natio":request.body.natio,
        "ecurie":request.body.ecurie,
        "points":request.body.points,
        "poids":request.body.poids,
        "taille":request.body.taille,
        "desc":request.body.desc,
    };

    let img = request.files.img;
    let imgName = img.name;    
    

    async.parallel([
        function(callback) {
            model.ajouterPilote(pilote, function(err, result) {
                let image = {
                    "phonum":1,
                    "pilnum":result.insertId,
                    "phoadresse":imgName,
                };

                modelPhotos.ajouterPhoto(image, function(err, result) {
                    callback(null, result);
                });            
            });
        },
        // Ici, on déplace l'image dans son dossier
        function(callback) {
            img.mv("../public/public/image/pilote/" + imgName);
            
            callback(null);
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionPilotes', response);

    });
 
};

module.exports.ModifierPilote = function(request, response) {
    response.title = 'Modification d\'un pilote';

    let pilote = {
        "num":request.body.id,
        "prenom":request.body.prenom,
        "nom":request.body.nom,
        "dateNais":request.body.dateNaisJour + "/" + request.body.dateNaisMois + "/" + request.body.dateNaisAnnee,
        "natio":request.body.natio,
        "ecurie":request.body.ecurie,
        "points":request.body.points,
        "poids":request.body.poids,
        "taille":request.body.taille,
        "desc":request.body.desc,
    };

    let img;
    let imgName;

    if (null != request.files) {
        img = request.files.img;
        imgName = img.name;    
    }else{
        imgName = request.body.imgOriginale;
    }

    

    async.parallel([
        function(callback) {
            model.modifierPilote(pilote, function(err, result) {
                callback(null, result);
            });
        },
        // Déplacement de la nouvelle image le cas échéant
        function(callback) {
            if (null != request.files) {
                let img = request.files.img;
                img.mv("../public/public/image/pilote/" + imgName);
            }

            callback(null);
        },
        // Modification de l'image
        function(callback) {
            let image = {
                "phonum":1,
                "pilnum":request.body.id,
                "phoadresse":imgName,
            };

            modelPhotos.modifierPhoto(image, function(err, result) {
                callback(null, result);
            });    
        },
        // Suppression du disque de l'ancienne image si modifiée
        function(callback) {
            if (null != request.files) {
                fs.unlink("../public/public/image/pilote/" + request.body.imgOriginale, (err) => {
                    if (err) throw err;
                    console.log(request.body.imgOriginale + ' supprimé');
                    callback(null, null);
                });
            }else{
                callback(null, null);
            }
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionPilotes', response);

    });
 
};

module.exports.SupprimerPilote = function(request, response) {
    response.title = 'Suppression d\'un pilote';

    let id = request.params.num;

    // On doit passer par des requêtes synchrones car la quatrième ne peut s'éxecuter sans les trois premières

    modelPhotos.supprimerPhotosParIdPilote(id, function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        modelCourse.supprimerCoursesByIdPilote(id, function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }

            modelSponsor.supprimerSponsorsDUnPilote(id, function(err, result) {
                if (err) {
                    // gestion de l'erreur
                    console.log(err);
                    return;
                }

                model.supprimerPilote(id, function(err, result) {
                    if (err) {
                        // gestion de l'erreur
                        console.log(err);
                        return;
                    }

                    response.suppression = "supp";
            
                    response.render('gestionPilotes', response);
                });
        
            });
            
    
        });

    });
     
};

module.exports.ModifierPhotos = function(request, response) {
    response.title = 'Modification des photos';

    let id = request.params.num;    

    async.parallel([
        function(callback) {
            modelPhotos.listerPhotosPilote(id, function(err, result) {
                callback(null, result);
            });
        },
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.photos = result[0];
        response.id = id;        
        
        response.render('gestionPhotos', response);

    });
 
};

module.exports.ModifierPhoto = function(request, response) {
    response.title = 'Modification des photos';

    let pilnum = request.params.pilnum;    
    let phonum = request.body.phonum;   

    let phoadresse;

    if (null != request.files) {
        phoadresse = request.files.img.name;
    }else{
        phoadresse = request.body.imgOriginale;
    }
    
    
    let photo = {
        "phonum":phonum,
        "pilnum":pilnum,
        "phosujet":request.body.sujet,
        "phocommentaire":request.body.comm,
        "phoadresse":phoadresse,
    };
    

    async.parallel([
        function(callback) {
            if (null != request.files) {
                let img = request.files.img;
                img.mv("../public/public/image/pilote/" + phoadresse);
            }

            callback(null);
        },
        // Suppression de la photo originale du disque en cas de changement
        function(callback) {
            if (null != request.files) {
                fs.unlink("../public/public/image/pilote/" + request.body.imgOriginale, (err) => {
                    if (err) throw err;
                    console.log(request.body.imgOriginale + ' supprimé');
                    callback(null, null);
                });
            }else{
                callback(null, null);
            }
        },
        function(callback) {
            modelPhotos.modifierPhoto(photo, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }


        response.modif = true;
        response.pilnum = pilnum;
        
        
        response.render('gestionPhotos', response);

    });
 
};

module.exports.SupprimerPhoto = function(request, response) {
    response.title = 'Modification des photos';

    let pilnum = request.params.pilnum;    
    let phonum = request.body.phonum;   


    let photo = {
        "phonum":phonum,
        "pilnum":pilnum,
    };
    
    
    async.parallel([
        // Suppression de la DB
        function(callback) {
            modelPhotos.supprimerPhoto(photo, function(err, result) {
                callback(null, result);
            });
        },
        // Suppression du disque
        function(callback) {
            fs.unlink("../public/public/image/pilote/" + request.body.imgOriginale, (err) => {
                if (err) throw err;
                console.log(request.body.imgOriginale + ' supprimé');
                callback(null, null);
            });
  
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }


        response.modif = true;
        response.pilnum = pilnum;        
        
        response.render('gestionPhotos', response);

    });
 
};

module.exports.AjouterPhoto = function(request, response) {
    response.title = 'Modification des photos';

    let pilnum = request.params.pilnum;    

    let phoadresse = request.files.img.name;
 

    async.parallel([
        function(callback) {
            let img = request.files.img;
            img.mv("../public/public/image/pilote/" + phoadresse);
            
            callback(null);
        },
        function(callback) {
            modelPhotos.getPlacePhoto(pilnum, function(err, result) {                
                
                let photo = {
                    "phonum":result[0].id,
                    "pilnum":pilnum,
                    "phosujet":request.body.sujet,
                    "phocommentaire":request.body.comm,
                    "phoadresse":phoadresse,
                };                

                modelPhotos.ajouterPhoto(photo, function(err, result) {
                    callback(null, result);
                });
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }


        response.modif = true;
        response.pilnum = pilnum;
        
        
        response.render('gestionPhotos', response);

    });
 
};