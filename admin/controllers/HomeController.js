// ////////////////////////////////////////////// A C C U E I L
let model = require('../models/login.js');
let async = require('async');

let Cryptr = require('cryptr');
let cryptr = new Cryptr('MaSuperCléDeChiffrementDeouF');

module.exports.Connexion = function(request, response) {

    let login;

    if (null != request.body.login) {
        login = {
            "login":request.body.login,
            "passwd":request.body.passwd,
        };
    }

    let deconnexion = request.params.deconnexion;

    
    
    response.title = "Veuillez vous connecter.";
    async.parallel([
            // Gestion de la connexion
            function(callback) {
                if (null != login) {
                    model.getPasswordWithLogin(login, function(err, result) {

                        // On chiffre puis déchiffre son mot de passe pour être sûr de la clef utilisée
                        let encryptedStringFromUser = cryptr.encrypt(login.passwd);
                        let decryptedStringFromUser = cryptr.decrypt(encryptedStringFromUser);

                        // On déchiffre le mot de passe de la DB
                        let decryptedStringFromDB = cryptr.decrypt(result[0].passwd);

                        // S'ils sont égaux, alors l'utilisateur est connecté
                        if (decryptedStringFromUser == decryptedStringFromDB) {                            
                            request.session.connected = true;
                        }
                        

                        callback(null, result);
                    });
                }else{
                    if (null != deconnexion) {
                        request.session.connected = false;
                    }
                    callback(null, null);
                }
            }
        ],
        function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }



            response.last = result[0];
            response.connexion = true;


            response.render('connexion', response);


        });
};

module.exports.NotFound = function(request, response) {
    response.title = "Bienvenue sur le site de SIXVOIX (IUT du Limousin).";
    response.render('notFound', response);
};