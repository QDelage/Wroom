// ////////////////////// L I S T E R     C I R C U I T S
let model = require('../models/circuit.js');
let modelPilote = require('../models/pilote.js');
let modelGP = require('../models/resultats.js')
let modelCourse = require('../models/course.js')
let async = require('async');
const fs = require('fs');



module.exports.ListerCircuit = function(request, response) {

    response.title = 'Gestion des circuits';


    async.parallel([
        function(callback) {
            model.getListeCircuit(function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.circuits = result[0];
        
        response.render('gestionCircuits', response);

    });
};

module.exports.AjoutForm = function(request, response) {
    response.title = 'Ajout d\'un circuit';

    let id = request.params.num; // Id du pilote à modifier

    async.parallel([
        // result[0]
        function(callback) {
            modelPilote.getAllPays(function(err, result) {
                callback(null, result);
            });
        },
        // result[1]
        function(callback) {
            model.getDetailCircuit(id, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }        

        response.pays = result[0];
        if (result[1]) {
            response.circuit = result[1][0];
        }

        response.render('gestionCircuits', response);

    });
 

};


module.exports.AjouterCircuit = function(request, response) {
    response.title = 'Ajout d\'un circuit';

    // On récupère le nom de l'image pour le mettre dans la DB
    let imgName = request.files.img.name;

    let circuit = {
        "nom":request.body.nom,
        "longueur":request.body.longueur,
        "pays":request.body.pays,
        "nbspectateurs":request.body.nbspectateurs,
        "img":imgName,
        "desc":request.body.desc,
    };
    

    async.parallel([
        // Ici, on déplace l'image dans son dossier
        function(callback) {
            let img = request.files.img;

            img.mv("../public/public/image/circuit/" + imgName);
            
            callback(null);
        },
        // Ici, on enregistre le circuit dans la DB
        function(callback) {
            model.ajouterCircuit(circuit, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionCircuits', response);

    });

}

module.exports.ModifierCircuit = function(request, response) {
    response.title = 'Modification d\'un circuit';

    // On récupère le nom de l'image pour le mettre dans la DB

    let imgName;
    
    if (null != request.files) {
        imgName = request.files.img.name;
    }else{
        imgName = request.body.imgCircuitOriginal;
    }

    let circuit = {
        "num":request.body.id,
        "nom":request.body.nom,
        "longueur":request.body.longueur,
        "pays":request.body.pays,
        "nbspectateurs":request.body.nbspectateurs,
        "img":imgName,
        "desc":request.body.desc,
    };
    

    async.parallel([
        function(callback) {
            // Ajout de la nouvelle image
            if (null != request.files) {
                let img = request.files.img;
                img.mv("../public/public/image/circuit/" + imgName);
            }

            callback(null);
        },
        function(callback) {
            // Suppression de l'ancienne image
            if (null != request.files) {
                fs.unlink("../public/public/image/circuit/" + request.body.imgCircuitOriginal, (err) => {
                    if (err) throw err;
                    console.log(request.body.imgCircuitOriginal + ' supprimé');
                });
            }
            callback(null, null);
        },
        function(callback) {
            model.modifierCircuit(circuit, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionCircuits', response);

    });
 
};

module.exports.SupprimerCircuit = function(request, response) {
    response.title = 'Suppression d\'un circuit';

    let id = request.params.num;

    let adresseImg; // Adresse de l'image à supprimer

    async.parallel([
        // Suppression de la DB
        // On doit passer par des requêtes synchrones car la troisième ne peut s'éxecuter sans les deux premières etc
        function(callback) {

            modelCourse.supprimerCoursesParIdCircuit(id, function(err, result) {
                if (err) {
                    // gestion de l'erreur
                    console.log(err);
                    return;
                }

            
                modelGP.supprimerGPParIdCircuit(id, function(err, result) {
                    if (err) {
                        // gestion de l'erreur
                        console.log(err);
                        return;
                    }

                    model.getDetailCircuit(id, function(err, result) {
                        if (err) {
                            // gestion de l'erreur
                            console.log(err);
                            return;
                        }

                        adresseImg  = result[0].ciradresseimage;                        
                        
                        model.supprimerCircuit(id, function(err, result) {
                            if (err) {
                                // gestion de l'erreur
                                console.log(err);
                                return;
                            }

                            fs.unlink("../public/public/image/circuit/" + adresseImg, (err) => {
                                if (err) throw err;
                                console.log(adresseImg + ' supprimé');
                                callback(null, null);
                            });
                    
                        });
            
                    });
            
                });

            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }        
    
        response.suppression = "supp";
                
        response.render('gestionCircuits', response);

    });

};