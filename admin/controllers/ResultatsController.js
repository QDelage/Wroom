// //////////////////////////L I S T E R    R E S U L T A T S
let model = require('../models/resultats.js');
let modelPilote = require('../models/pilote.js');
let modelCourse = require('../models/course.js');
let async = require('async');

module.exports.MenuResultats = function(request, response) {

    response.title = 'Saisie des résulats des grands prix';
    model.getListeGrandPrix(function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }        
        response.gps = result;

        

        response.render('gestionResultat', response);
    });
};


module.exports.SaisieResultats = function(request, response) {
    response.title = 'Saisie des résulats';

    let gp = request.body.gp;
    let supp = request.body.supp;
    
    let resultat = null;

    // Si on a saisi un résultat
    if (request.body.pilote) {
        resultat = {
            "gpnum":gp,
            "pilnum":request.body.pilote,
            "tempscourse":request.body.m + ":" + request.body.s + ":" + request.body.ms,
        };
        
    }
    

    async.parallel([
            // result 0
            function(callback) {
                // Ici, on insère le résultat récupéré puis le classement, ou la suppression puis le classement, ou directement le classement
                if (null != resultat) {
                    modelCourse.ajouterResultatCourse(resultat, function(err, result) {
                        model.getClassementParGP(gp, function(err, result) {
                            callback(null, result);
                        });
                    });
                }else if (0 != supp){                    
                    modelCourse.supprimerResultatCourse(supp, gp, function(err, result) {
                        model.getClassementParGP(gp, function(err, result) {
                            callback(null, result);
                        });
                    });
                }else {
                    model.getClassementParGP(gp, function(err, result) {
                        callback(null, result);
                    });
                }
            },
            // result 1
            function(callback) {
                model.getPointsParRang(function(err, result) {
                    callback(null, result);
                });
            },
            // result 2
            function(callback) {
                modelPilote.getAllPilotes(function(err, result) {
                    callback(null, result);
                });
            }
        ],
        function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }



            response.gp = result[0];
            response.points = result[1];
            response.pilotes = result[2];
            response.gpnum = gp;
            response.render('gestionResultat', response);


        });

}