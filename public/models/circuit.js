/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Récupérer l'intégralité des circuits avec l'adresse de la photo du pays du circuit
 * @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
 */
module.exports.getListeCircuit = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT cirnum, payadrdrap, ciradresseimage, cirnom FROM " +
                "circuit c INNER JOIN pays p " +
                "ON p.paynum=c.paynum ORDER BY cirnom";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer les détails d'un circuit afin de les afficher
 * @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
 */
module.exports.getDetailCircuit = function(cirnum, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT cirnum, paynom, cirnom, cirlongueur, cirnbspectateurs, " +
                "ciradresseimage, cirtext " +
                "FROM circuit c " +
                "JOIN pays p ON c.paynum = p.paynum " +
                "WHERE cirnum = " + cirnum;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};