/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Récupérer les premieres lettres des noms de chaque pilotes présents dans la base
 * @return Un tableau qui contient la lettre
 */
module.exports.getLettresPilotes = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT distinct substr(pilnom,1,1) as lettre FROM pilote " +
                "ORDER BY 1";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer les premieres lettres des noms de chaque pilotes présents dans la base
 * @return Un tableau qui contient la lettre
 */
module.exports.getPilotesParLettre = function(lettre, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT pi.pilnum, pilprenom, pilnom, phoadresse FROM pilote pi " +
                "JOIN photo ph ON pi.pilnum = ph.pilnum " +
                "WHERE pilnom LIKE '" + lettre + "%' " +
                "AND phonum = 1 " +
                "ORDER BY 1";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir les infos d'un pilote selon son id
 */
module.exports.getPiloteParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT pi.pilnum, pilprenom, pilnom, " +
                "pilpoids, piltaille, paynom, piltexte,  " +
                "DATE_FORMAT(pildatenais, \" %W %d %M %Y \") as pildatenais " +
                "FROM pilote pi " +
                "JOIN photo ph ON pi.pilnum = ph.pilnum " +
                "JOIN pays pa ON pi.PAYNUM = pa.PAYNUM " +
                "WHERE pi.pilnum = " + id + " ";
            connexion.query(sql, callback);




            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};



/*
 * Obtenir les sponsors d'un pilote
 */
module.exports.getSponsorsParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT sp.* FROM pilote p " +
                "JOIN sponsorise s ON p.PILNUM = s.PILNUM " +
                "JOIN sponsor sp ON s.SPONUM = sp.SPONUM " +
                "WHERE p.pilnum = " + id + " ";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};