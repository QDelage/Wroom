/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Récupérer l'intégralité des résultats avec l'adresse de la photo du pays du circuit
 * @return
 */
module.exports.getListeGrandPrix = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT gpnum, g.cirnum, gpnom, gpdate, gpnbtours, payadrdrap " +
                "FROM grandprix g " +
                "INNER JOIN circuit c ON g.cirnum=c.cirnum " +
                "JOIN pays p ON p.paynum = c.paynum " +
                "ORDER BY gpnom";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};


/*
 * Récupérer le classement d'un GrandPrix
 * @return
 */
module.exports.getClassementParGP = function(data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT pilnom, pilprenom, tempscourse " +
                " " +
                "FROM grandprix g " +
                "JOIN course c ON g.gpnum=c.gpnum " +
                "JOIN pilote p ON p.pilnum = c.pilnum " +
                "WHERE g.gpnum = " + data + " " +
                "ORDER BY tempscourse ASC " +
                "LIMIT 10";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer le nombre de points selon le rang
 * @return
 */
module.exports.getPointsParRang = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT ptplace, ptnbpointsplace " +
                "FROM points pts " +
                "ORDER BY ptnbpointsplace DESC";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer la description d'un grandprix
 * @return
 */
module.exports.getDescriptionParResultat = function(data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT gpcommentaire " +
                "FROM  grandprix gp " + 
                "WHERE gpnum = " + data;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Récupérer le dernier résultats
 * @return
 */
module.exports.getLastRank = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql =
            "SELECT gp.gpnom, DATE_FORMAT(gp.gpdate, \"%d/%m/%Y\") as DATE, gp.gpnum, DATE_FORMAT(gp.gpdatemaj, \"%d/%m/%Y\") as DATEMAJ " +
            "FROM  grandprix gp " +
            "INNER JOIN ( SELECT MAX(gpdate) AS gpdate " +
	            "FROM grandprix gp) b " +
            "ON gp.gpdate = b.gpdate";

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};