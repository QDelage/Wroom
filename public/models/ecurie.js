/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */

let db = require('../configDb');

/*
 * Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
 * @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
 */
module.exports.getListeEcurie = function(callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT ecunum, payadrdrap, ecunom FROM " +
                "ecurie e INNER JOIN pays p ";
            sql = sql + "ON p.paynum=e.paynum ORDER BY ecunom";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir l'écurie d'un pilote selon son id
 */
module.exports.getEcurieParId = function(id, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT ecunom " +
                "FROM pilote pi JOIN ecurie e ON pi.ecunum = e.ecunum " +
                "WHERE pi.pilnum = " + id + " ";
            connexion.query(sql, callback);



            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};



/*
 * Obtenir les détails d'une écurie selon son id
 */
module.exports.getDetailEcurie = function(ecunum, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT ecunum, fpnum, ecunom, ecunomdir, ecuadrsiege, " +
                "ecupoints, paynom, ecuadresseimage " +
                "FROM ecurie e " +
                "JOIN pays p ON e.paynum = p.paynum " +
                "WHERE ecunum = " + ecunum;
            connexion.query(sql, callback);



            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir les pilotes d'une écurie
 */
module.exports.getPilotesParEcurie = function(ecunum, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT p.pilnum, pilnom, pilprenom, SUBSTRING(piltexte, 1, 100) as piltexte, phoadresse " +
                "FROM ecurie e " +
                "JOIN pilote p ON e.ecunum = p.ecunum " +
                "JOIN photo ph ON p.pilnum = ph.pilnum " +
                "WHERE phonum = 1 " +
                "AND p.ecunum = " + ecunum;

            connexion.query(sql, callback);



            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

/*
 * Obtenir les voitures d'une écurie
 */
module.exports.getVoituresParEcurie = function(ecunum, callback) {
    // connection à la base
    db.getConnection(function(err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            let sql = "SELECT voinum, v.ecunum, " +
                "voinom, voiadresseimage, typelibelle " +
                "FROM ecurie e " +
                "JOIN voiture v ON e.ecunum = v.ecunum " +
                "JOIN type_voiture t ON t.typnum = v.typnum " +
                "WHERE e.ecunum = " + ecunum;
            connexion.query(sql, callback);



            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};