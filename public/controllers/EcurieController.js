let model = require('../models/ecurie.js');
let async = require('async');

// //////////////////////// L I S T E R  E C U R I E S

module.exports.ListerEcurie = function(request, response) {
    response.title = 'Liste des écuries';
    model.getListeEcurie(function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeEcurie = result;
        response.render('listerEcurie', response);
    });
}

module.exports.AffichageEcurie = function(request, response) {
    response.title = 'Détail écurie';

    let data = request.params.num;

    async.parallel([
            // result 0
            function(callback) {
                model.getListeEcurie(function(err, result) {
                    callback(null, result);
                });
            },
            // result 1
            function(callback) {
                model.getDetailEcurie(data, function(err, result) {
                    callback(null, result);
                });
            },
            // result 2
            function(callback) {
                model.getPilotesParEcurie(data, function(err, result) {
                    callback(null, result);
                });
            },
            // result 3
            function(callback) {
                model.getVoituresParEcurie(data, function(err, result) {
                    callback(null, result);
                });
            }
        ],
        function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }


            response.listeEcurie = result[0];

            response.detailEcurie = result[1];

            response.listePilotes = result[2];

            response.listeVoitures = result[3];

            response.render('listerEcurie', response);


        });



}