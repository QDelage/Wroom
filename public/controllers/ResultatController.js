// //////////////////////////L I S T E R    R E S U L T A T S
let model = require('../models/resultats.js');
let async = require('async');

module.exports.ListerResultat = function(request, response) {

    response.title = 'Liste des résulats des grands prix';
    model.getListeGrandPrix(function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeGP = result;

        response.render('listerResultat', response);
    });
};


module.exports.AffichageResultat = function(request, response) {
    response.title = 'Détail des grand prix';

    let data = request.params.num;

    async.parallel([
            // result 0
            function(callback) {
                model.getListeGrandPrix(function(err, result) {
                    callback(null, result);
                });
            },
            // result 1
            function(callback) {

                model.getClassementParGP(data, function(err, result) {
                    callback(null, result);
                });
            },
            // result 2
            function(callback) {
                model.getPointsParRang(function(err, result) {
                    callback(null, result);
                });
            },
            // result 3
            function(callback) {
                model.getDescriptionParResultat(data, function(err, result) {
                    callback(null, result);
                });
            }
        ],
        function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }



            response.listeGP = result[0];
            response.detailRes = result[1];
            response.points = result[2];
            response.desc = result[3][0];
            response.render('listerResultat', response);


        });

}