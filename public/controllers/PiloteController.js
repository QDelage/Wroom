// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S
let model = require('../models/pilote.js');
let modelEcurie = require('../models/ecurie.js');
let modelPhotos = require('../models/photos.js');



let async = require('async');

module.exports.Repertoire = function(request, response) {
    response.title = 'Répertoire des pilotes';



};

module.exports.RepertoireParLettre = function(request, response) {
    response.title = 'Répertoire des pilotes';



};

module.exports.AffichagePilotes = function(request, response) {
    response.title = 'Répertoire des pilotes';

    let data = request.params.lettre;

    async.parallel([
        function(callback) {
            model.getLettresPilotes(function(err, result) {
                callback(null, result);
            });
        },
        function(callback) {

            model.getPilotesParLettre(data, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeLettresPilotes = result[0];
        response.listePilotes = result[1];
        response.render('repertoirePilotes', response);

    });

};

module.exports.AffichagePilote = function(request, response) {
    response.title = 'Répertoire des pilotes';

    let data = request.params.lettre;
    let data2 = request.params.num;


    async.parallel([
        // result 0
        function(callback) {
            model.getLettresPilotes(function(err, result) {
                callback(null, result);
            });
        },
        // result 1
        function(callback) {
            model.getPiloteParId(data2, function(err, result) {
                callback(null, result);
            });

        },
        // result 2
        function(callback) {
            model.getSponsorsParId(data2, function(err, result) {
                callback(null, result);
            });
        },
        // result 3
        function(callback) {
            modelPhotos.getPhotosParId(data2, function(err, result) {
                callback(null, result);
            });
        },
        // result 4
        function(callback) {
            modelEcurie.getEcurieParId(data2, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.listeLettresPilotes = result[0];

        response.photos = result[3];

        response.photoPrinc = result[3][0].phoadresse;
        response.pilNom = result[1][0].pilnom + " " + result[1][0].pilprenom;
        response.pilDate = result[1][0].pildatenais;
        if (result[4][0]) {
            response.ecurie = result[4][0].ecunom;
        }
        response.poids = result[1][0].pilpoids;
        response.taille = result[1][0].piltaille;
        response.paynom = result[1][0].paynom;
        response.whoami = result[1][0].piltexte;

        response.sponsors = result[2];



        response.render('repertoirePilotes', response);

    });





};