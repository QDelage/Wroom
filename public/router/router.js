let HomeController = require('./../controllers/HomeController');
let ResultatController = require('./../controllers/ResultatController');
let EcurieController = require('./../controllers/EcurieController');
let PiloteController = require('./../controllers/PiloteController');
let CircuitController = require('./../controllers/CircuitController');

// Routes
module.exports = function(app) {

    // Main Routes
    app.get('/', HomeController.Index);
    app.get('/accueil', HomeController.Index);

    // pilotes
    app.get('/repertoirePilote', PiloteController.AffichagePilotes);
    // route dynamique par lettre
    app.get('/repertoirePilote/:lettre', PiloteController.AffichagePilotes);
    // route dynamique par pilote
    app.get('/repertoirePilote/pilote/:num', PiloteController.AffichagePilote);



    // circuits
    app.get('/circuits', CircuitController.ListerCircuit);
    app.get('/detailCircuit/:num', CircuitController.AffichageCircuit);


    // Ecuries
    app.get('/ecuries', EcurieController.ListerEcurie);
    app.get('/detailEcurie/:num', EcurieController.AffichageEcurie);

    //Résultats
    app.get('/resultats', ResultatController.ListerResultat);
    app.get('/detailGrandPrix/:num', ResultatController.AffichageResultat);




    // tout le reste
    app.get('*', HomeController.NotFound);
    app.post('*', HomeController.NotFound);

};