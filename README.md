# W R O O M

Projet en NodeJS d'un site web de formule 1. Réalisé dans le cadre du DUT Informatique, avec structure et jeu d'essai fournie par l'IUT.

## Pour commencer

Le fichier grandprix.sql vous permettra de créer votre base grandprix.

Pour démarrer, démarrer les parties publiques et admin, dans chaque dossier, "node app", APRÈS avoir installé les modules nécessaires à chaque partie "npm install".

⊂(◉‿◉)つ

### Prérequis

Logiciels requis pour l'utilisation


* MySQL
* Node

### Installation


Installation de la base de données

```
Fichier grandprix.sql
```

Configuration de l'application

```
Fichiers configDb.js de chaque partie
```

## Développé avec

* [MySQL](https://www.mysql.com/) - Le SGBD utilisé
* [NodeJS](https://nodejs.org/) - Environnement d'exécution JavaScript

## Auteurs

* **IUT du Limousin** - *Travail initial* - Équipe enseignante
* **Quentin DELAGE** - *Développement* - [Dylage](https://gitlab.com/Dylage)